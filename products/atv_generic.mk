#
# Copyright (C) 2014 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_PACKAGES := \
    TelephonyProvider \
    rild

ifeq ($(USE_QTI_TV_APP), true)
# packages in custom LiveTv
PRODUCT_PACKAGES += \
    LiveTvQti \
    icu4j-usbtuner-qti \
    tv-common-qti \
    libtunertvinputqti_jni \
    libminijail_jni
# Custom exoplayer library
PRODUCT_PACKAGES += \
    lib-exoplayer-qti \
    lib-exoplayer-v2-qti
else
# packages in default LiveTv
PRODUCT_PACKAGES += \
    LiveTv \
    icu4j-usbtuner \
    tv-common \
    libtunertvinput_jni
endif # USE_QTI_TV_APP

# Put en_US first in the list, so make it default.
PRODUCT_LOCALES := en_US

# Include drawables for various densities.
PRODUCT_AAPT_CONFIG := normal large xlarge tvdpi hdpi xhdpi xxhdpi
PRODUCT_AAPT_PREF_CONFIG := xhdpi

# From build/target/product/full_base.mk
PRODUCT_PACKAGES += \
    LiveWallpapersPicker \
    PhotoTable

PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml \
    frameworks/av/media/libstagefright/data/media_codecs_google_tv.xml:system/etc/media_codecs_google_tv.xml

# Netflix permission
PRODUCT_COPY_FILES += \
    device/qcom/apq8098_latv/permissions/nrdp.modelgroup.xml:system/etc/permissions/nrdp.modelgroup.xml

$(call inherit-product, device/qcom/apq8098_latv/products/atv_base.mk)
